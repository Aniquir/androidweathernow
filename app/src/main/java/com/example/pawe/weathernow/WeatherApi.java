package com.example.pawe.weathernow;

import com.example.pawe.weathernow.api.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Paweł on 2017-10-14.
 */

public interface WeatherApi {
    @GET("weather")
    Call<WeatherResponse> getWeather(@Query("appid") String appid, @Query("q") String location);

    @GET("weather")
    Call<WeatherResponse> getWeather(@Query("appid") String appid, @Query("lat") Double lat, @Query("lon") Double lon);
}
